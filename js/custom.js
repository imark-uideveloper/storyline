(function($) {
  'use strict';
  
  $(document).ready(function(){
    
    // Wow Js
    var wow = new WOW({
      
        mobile: false // default
    })
    wow.init();
    
    //Sticky Header
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            $(".header").addClass("fixed-header");
        } else {
            $(".header").removeClass("fixed-header");
        }
    });
    
    //Header

    $(window).on("load resize scroll", function(e) {
      var Win = $(window).height();
      var Header = $("header").height();
      var Footer = $("footer").height();

      var NHF = Header + Footer;

      $('.main').css('min-height', (Win - NHF));

    });
    
    // My Booking
    
    jQuery('.info-sec').each(function () {
        var infoForm = jQuery(this).find('.form-sec');
        var contentInfo = jQuery(this).find('.content-sec');
        var editBtn = jQuery(this).find('.edit');
        infoForm.slideUp();
        editBtn.click(function () {
          setTimeout(function(){
            editBtn.parent().parent().toggleClass("infoFormOpen");
          },300)
            
            infoForm.slideToggle();
            contentInfo.slideToggle();
        });
    });
    
    // Client Slider
    var clientlogo = $('body').find('.client-logo-slider');
    clientlogo.slick({
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<a class="slick-prev"><i class="fa fa-long-arrow-left"></i></a>',
        nextArrow: '<a class="slick-next"><i class="fa fa-long-arrow-right"></i></a>',
    });
    
    // Particular Slider
    var particulars = $('body').find('.particular-slider');
    particulars.slick({
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<a class="slick-prev"><i class="fa fa-long-arrow-left"></i></a>',
        nextArrow: '<a class="slick-next"><i class="fa fa-long-arrow-right"></i></a>',
    });
    
    // Testimonials
    var testimonials = $('body').find('.testimonial-section');
    testimonials.owlCarousel({
        loop:false,
        margin: 25,
        nav:true,
        autoPlay : true,
        dots:false,
        navText : ["<i class='fa fa-long-arrow-left'></i>","<i class='fa fa-long-arrow-right'></i>"],
        responsive:{
            0:{
                items:1
            },
           600:{
                items:1,
            },
            1000:{
                items:2,
            }
        }
    });

    //Model
    $(function() {
        //----- OPEN
        $('[data-popup-open]').on('click', function(e) {
        var targeted_popup_class = $(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);
    
        e.preventDefault();
        });
    
        //----- CLOSE
        $(document).on('click','[data-popup-close]','.cstmClose',function(e) {
        var targeted_popup_class = $(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);
    
        e.preventDefault();
        });
    });

    //Textarea 

    $('.firstCap, textarea').on('keypress', function(event) {
        var jQuerythis = $(this),
            thisVal = jQuerythis.val(),
            FLC = thisVal.slice(0, 1).toUpperCase(),
        con = thisVal.slice(1, thisVal.length);
        $(this).val(FLC + con);
    });
    
    
    // Profile Upload
    
    function readURL(input) { 
        if (input.files && input.files[0]) {
        var reader = new FileReader();
            reader.onload = function (e) {
                $('.profile-img-sec figure').attr('style', 'background-image: url('+e.target.result   +')');  
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('body').find(".profile-img-sec input[type='file']").on('change',function(){
        readURL(this);
    });
    
    // Upload Image
    
    function readsURL(input) { 
        console.log(input.files["0"].name);
        if (input.files && input.files[0]) {
        var uimage = new FileReader();
            uimage.readAsDataURL(input.files[0]);
             $(".user-content #uploadFile").val(input.files["0"].name);
        }
    }
    $('body').find(".user-profile-wrap .user-content #uploadFile").on('change',function(){
        readsURL(this);
    });
    
    
    // Date picker
    $('body').find('[data-toggle="datepicker"]').datepicker({
      container: ".docs-datepicker-container",
      inline:true,
    });

  });
  
  // Custom Scrollbar

  $(window).on("load", function () {
      $(".inbox-list ul, .chat-box-wrap .chat-box-sec").mCustomScrollbar();
  });
  
  
  //Page Zoom
  document.documentElement.addEventListener('touchstart', function (event) {
    if (event.touches.length > 1) {
      event.preventDefault();
    }
  }, false);

  //Avoid pinch zoom on iOS
  document.addEventListener('touchmove', function(event) {
    if (event.scale !== 1) {
      event.preventDefault();
    }
  }, false);
})(jQuery)